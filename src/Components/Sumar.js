import React, { useState } from "react";
import Hanna from "../asset/MainScreen/Hanna.jpg";

export default function Sumar(){
    
    const [datos, setDatos] = useState({nombre: "Hannah Smith", usuario: "@hanna_super" , followers: 787, following:787 ,
    about: "👜 Fashion, beauty, lifestyle & aesthetic inspo 🙌 Founder of @beatyhstudio 📍 Berlin"})
    const [miVariable,setMiVariable] =useState(0)
    const [numero,setNumero]= useState(2)
    return(
    <>
        <img src={Hanna} />

        <div>

            <h1>{datos.nombre} - {miVariable}</h1>
            <input type="text" onChange={(event)=> setNumero(Number(event.target.value))}/>
            <button onClick={()=> setMiVariable(miVariable +numero)}> sumar </button>
            <button onClick={()=> setMiVariable(miVariable - numero)}> restar </button>
            <button onClick={()=> setMiVariable(miVariable * numero)}> multiplicar </button>
            <button onClick={()=> setMiVariable(miVariable / numero)}> dividir </button>
        </div>
    </>
    )
}