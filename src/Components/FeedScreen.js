import React, { useState } from "react";
import styles from "./../styles/FeedScreen.module.css";

export default function FeedScreen() {
  const [fotos, setfotos] = useState([
    {
      id: 1,
      link: "https://picsum.photos/200/300",
    },
    {
      id: 2,
      link: "https://picsum.photos/200/300",
    },
    {
      id: 3,
      link: "https://picsum.photos/200/300",
    },
    {
      id: 4,
      link: "https://picsum.photos/200/300",
    },
    {
      id: 5,
      link: "https://picsum.photos/200/300",
    },
    {
      id: 6,
      link: "https://picsum.photos/200/300",
    },
  ]);

  return (
    <>
      <div className={styles.box}>
        {fotos.map((fot, index) => (
          <img className={styles.box__images} src={fot.link} />
        ))}
      </div>
    </>
  );
}
