import React, { useState } from "react";
import Hanna from "../asset/MainScreen/Hanna.jpg";
import send from "../asset/MainScreen/send.jpg";
import more from "../asset/MainScreen/more.jpg";
import styles from "../styles/MainScreen.module.css";

export default function MainScreen({setMostrar}){

    const [datos, setDatos] = useState({nombre: "Hannah Smith", usuario: "@hanna_super" , followers: 787, following:787 ,
    about: "👜 Fashion, beauty, lifestyle & aesthetic inspo 🙌 Founder of @beatyhstudio 📍 Berlin"})


    return (
        <>
            

            <div className={styles.MainScreen}>

                <img src={Hanna} className={styles.MainScreen__Img}/>
                
                <h1>{datos.nombre}</h1>
                <h4>{datos.usuario}</h4>
             


            </div>

            <div className={styles.MainScreen__Follow__Menu}>
                
                <button name="Follow"> <h4>+ Follow</h4></button>

                <button name="DM"> <img src={send} /></button>

                <div><h4>{datos.followers}</h4> Followers</div>

                <div><h4>{datos.following}</h4> Following</div>

                <button name="More"> <img src={more}/></button>

            </div>    

            <div className={styles.MainScreen__Inf} > 

                <button onClick={()=> setMostrar("info")}> <h4> Info</h4></button>

                <button onClick={()=> setMostrar("feed")}> <h4>+ Feed</h4></button>

                <button onClick={()=> setMostrar("togged")} > <h4>+ Tagged</h4></button>
            </div>
            

        </>


    )
}



