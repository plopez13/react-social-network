import React, { useState } from "react";
import Styles from "../styles/InfoScript.module.css";
import Instagram from "../asset/InfoScript/Instagram.svg";
import Twitter from "../asset/InfoScript/Twitter.svg";
import Youtube from "../asset/InfoScript/Youtube.svg";
import Facebook from "../asset/InfoScript/Facebook.svg";
import Telegram from "../asset/InfoScript/Telegram.svg";

export default function InfoScript() {
  const [redes, setredes] = useState([
    {
      red: "Instagram",
      link: Instagram,
      hashtag: "@hannah_super",
      estilo: "naranja",
    },
    {
      red: "Twitter",
      link: Twitter,
      hashtag: "@hannah_super",
      estilo: "azul",
    },
    {
      red: "Youtube",
      link: Youtube,
      hashtag: "@hannah_super",
      estilo: "rojo",
    },
    {
      red: "Facebook",
      link: Facebook,
      hashtag: "",
      estilo: "azul",
    },
    {
      red: "Telegram",
      link: Telegram,
      hashtag: "@hannah_super",
      estilo: "azul",
    },
  ]);

  function className(params) {
    let classN = `${Styles.boton} `;
    if (params === "rojo") {
        classN += `${Styles.boton__rojo}`;
    } else if (params === "naranja") {
      classN += `${Styles.boton__naranja}`;
    } else {
        classN += `${Styles.boton__azul}`;
    }

    return classN;
}


  return (
    <>
      <div className={Styles.contenedor}>
        <div className={Styles.contenedor__cont}>
          <div className={Styles.contenedor__cont__h5}>About</div>
          <h5 className={Styles.contenedor__cont__h5}>👜 Fashion, beauty, lifestyle & aesthetic inspo </h5>
          <h5 className={Styles.contenedor__cont__h5}>🙌 Founder of @beatyhstudio</h5>
          <h5 className={Styles.contenedor__cont__h5}>📍 Berlin</h5>
        </div>

        <div className={Styles.botones}>
          {redes.map((red, index) => (
            <button className={className(red.estilo)}>
              <img src={red.link} alt="imagen de producto"></img>
              <h5 className={Styles.botones__red}> {red.red}</h5>
              <h5 className={Styles}> {red.hashtag}</h5>
            </button>
          ))}
        </div>
      </div>
    </>
  );
}