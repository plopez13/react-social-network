import "./App.css";
import MainScreen from "./components/MainScreen";
import FeedScreen from "./components/FeedScreen";
import InfoScript from "./components/InfoScript";
import { useState } from "react";

function App() {

  const [mostrar, setMostrar] = useState("info")

  return (
    <div className="App">
      <MainScreen setMostrar = {setMostrar}/>
      {
        mostrar == "info" &&
        <InfoScript />
      }

      {
        mostrar == "feed" &&
        <FeedScreen />
      }


    </div>
  );
}

export default App;
